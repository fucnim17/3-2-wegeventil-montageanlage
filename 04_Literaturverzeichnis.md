Literaturverzeichnis
=========================

Agerer, M. S. (o. D.). Maschinenbau-Wissen. Maschinenbau-Wissen. Abgerufen am 7. März 2022 von:  
https://www.maschinenbau-wissen.de/skript3/mechanik/kinetik/283-rollreibung

Balluff GmbH. (o. D.). Balluff. Abgerufen am 1. Oktober 2021 von:  
https://www.balluff.com/local/at/home/

BBS 2 Wolfsburg. (o. D.). Grundlagen der Robotik - Koordinatensysteme und Verfahrensarten. Abgerufen am 12. Jänner 2022 von:  
https://www.xplore-dna.net/mod/page/view.php?id=449

Böge, A., Böge, G. \& Böge, W. (2016). Aufgabensammlung Technische Mechanik (23. Aufl. 2016 Aufl.). Springer Vieweg.  

Dillinger, J., et al. (2017). Fachkunde Metall - Mechanische Technologie (57. Auflage). Europa-Lehrmittel.  

Fibro. (o. D). Säulenführungsgestelle. Abgerufen am 23. Juli 2021 von:  
https://www.fibro.de/fileadmin/FIBRO/DE/02_NORMALIEN/Kapitel_A/PDF/B2_HK_DE_Kapitel_A.pdf

Industr. (12.6.2018). Antriebe im Vergleich - Die ideale Roboterzange wählen. Abgerufen am 15. August 2021 von:  
https://www.industr.com/de/pneumohydraulik-versus-elektromechanik-2310592

Kuka1. (18.10.2021). KUKA KR 10 R1100-2. Abgerufen am 12. Jänner 2022 von:  
https://www.kuka.com/-/media/kuka-downloads/imported/6b77eecacfe542d3b736af377562ecaa/0000290003\_en.pdf

Kuka2. (o. D.). KUKA.Sim. Abgerufen am 7. Jänner 2022 von:  
https://www.kuka.com/de-de/produkte-leistungen/robotersysteme/software/planung-projektierung-service-sicherheit/kuka\_sim

Kuka3. (o.D.). KUKA.SafeOperation. Abgerufen am 12. Jänner 2022 von:  
https://www.kuka.com/de-de/produkte-leistungen/robotersysteme/software/querschnittstechnologien/kuka\_safeoperation

Rechner-Sensors. (o. D.). IO-Link: Einfach verständlich erklärt. Abgerufen am 8. März 2022 von:  
https://www.rechner-sensors.com/dokumentation/wissen/io-link-einfach-verstaendlich-erklaert  

Schweizer, A. S. (o. D.). Haftreibungswerte Tabelle. schweizer-fn. Abgerufen am 13. Juli 2021 von:  
https://www.schweizer-fn.de/stoff/reibwerte/reibwerte.php#metallwerkstoffe

Steinel. (01.09.2021). Steinel Säulengestelle Katalogauszug. Abgerufen am 23. Juli 2021 von:  
https://www.steinel.com/fileadmin/user_upload/STEINEL-KA-Werkzeuggestelle-Standardgestelle_de.pdf

Tox-Pressotechnik1. (o. D.). Pneumohydraulische Antriebe. Abgerufen am 20. Juli 2021 von:  
https://at.tox-pressotechnik.com/produkte/antriebe/pneumohydraulische-antriebe/uebersicht-\\pneumohydraulische-antriebe/

Tox-Pressotechnik2. (01.11.2020). TOX-ElectricDrive Typ EQ-K Typenblatt 40.40. Abgerufen am 23 Juli 2021 von:  
https://de.tox-pressotechnik.com/assets/countries/DE/pdf/TOX\_TB\_4040\_EQ-K\_de.pdf

Tox-Pressotechnik3. (o. D.). Zubehör für das Tox-Kraftpaket. Abgerufen am 20. Juli 2021 von:  
https://at.tox-pressotechnik.com/produkte/antriebe/pneumohydraulische-antriebe/zubehoer/

Tox-Pressotechnik4. (01.03.2021). TOX-Kraftpaket Zubehör Typenblatt 10.10. Abgerufen am 23 Juli 2021 von:  
https://at.tox-pressotechnik.com/assets/countries/DE/pdf/TOX_TB_1010_de.pdf

VDI 3633. (2018). Simulation von Logistik-, Materialfluss- und Produktionssystemen - Begriffe. VDI-Fachbereich Fabrikplanung und -betrieb  

Warschat, J. \& Wagner, F. (o. D.). Einführung in die Simulationstechnik. Universität Stuttgart - Institut für Arbeitswissenschaft und Technologiemanagement (IAT)  

Zimmer Group. (o. D.). Zimmer Group. Abgerufen am 27. September 2021 von:  
https://www.zimmer-group.com/de/technologien-komponenten/komponenten/handhabungstechnik/greifer/pneumatisch
